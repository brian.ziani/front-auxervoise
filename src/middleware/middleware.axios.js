import axios from 'axios'
import {useState} from 'react';

export const baseURL = "https://api.auxervoise.fr";

const instance = axios.create({
    baseURL : baseURL,
    timeout : 2000
});

export const dataHandler = (location) => {
    const [data, setData] = useState({});
    const [filter, setFilter] = useState([])
    
    const getFilters = async () => {
        let slug = location.search.split('=');
        const req = slug.length < 2 ? slug[1] : slug[1].split('&')[0];
        await instance.get(location.pathname+req)
        .then(res => {
            setFilter(res.data)
        }).catch(err => {
            console.log(err);
        });
    }

    const getData = async () => {
        await instance.get(location.pathname+location.search)
        .then(res => {
            setData(res.data)
        }).catch(err => {
            console.log(err);
        });
    }
    
    return {
        data,
        filter,
        getFilters,
        getData,
    }
}
