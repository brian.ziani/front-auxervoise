import React, {Suspense, useState} from 'react';

import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Loading from '../components/utils/loading';

const Home = React.lazy(() => import('../components/home/'));
const Catalogue = React.lazy(() => import('../components/catalogue/'));
const Product = React.lazy(() => import('../components/product/'));
const Dev = React.lazy(() => import('../components/dev/'));

const Routes = () => {
    return (
        <Suspense fallback={<Loading/>}>
            <Router>
                <Switch>
                    <Route exact path='/' render={() => <Home/>}/>
                    <Route exact path='/menu/catalogue' render={() => <Catalogue/>}/>
                    <Route exact path='/menu/catalogue/:slug' render={() => <Product/>}/>
                    <Route exact path='/devcontact' render={() => <Dev/>}/>
                </Switch>
            </Router>
        </Suspense>
    );
};

export default Routes
