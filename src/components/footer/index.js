import React from 'react'
import {Link} from 'react-router-dom';
const Footer = () => {
    return (
        <footer className="w-full  mt-2 p-5 bottom-0 relative">
            <div className="text-gray-400 text-xs">Nous vous rappelons que :
            
                    <p className="pb-1">Les photos de la carte sont non contractuel 📷</p>
                    <p className="pb-1">La vente d'alcool est interdite aux mineurs 🔞</p>
                    <p className="pb-1">La consommation d'alcool est déconseillée aux femmes enceintes 🙅🏻‍♀️</p>
                    <p className="pb-1">L'établissement se réserve le droit de refuser l'accès aux personnes déjà fortemments alcoolisées ⛔</p>    
            </div>
            <Link to="/devcontact">
                <p className=" mt-2 text-xs text-center text-gray-400">Web App created by Brian Ziani</p>
            </Link>
        </footer>
    )
}

export default Footer
