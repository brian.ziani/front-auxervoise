import React from 'react'

const Dev = () => {
    return (
        <div>
            <h1 className="text-xl text-center">Contact :</h1>
            <p className="text-lg text-center">brian.ziani@gmail.com</p>
        </div>
    )
}

export default Dev
