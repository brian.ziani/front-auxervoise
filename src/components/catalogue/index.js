import React, {useEffect, useState} from 'react'
import axios from 'axios';
import {Link} from 'react-router-dom';
import {useLocation} from 'react-router-dom';
import Cards from './cards'
import Filters from './filters'
import Nav from '../nav'
import Footer from '../footer'
import { baseURL } from '../../middleware/middleware.axios';
const Catalogue = () => {
    let location = useLocation();
    const [data, setData] = useState([]);
    const [filter, setFilter] = useState([])
    const [animation, setAnimation] = useState({fade : 'animate-fade-in'})
    useEffect(() => {
        getData(location);
        getFilters(location);
    }, [location]);

    useEffect(() => {
        const timeout = setInterval(() => {
            if (animation.fade === 'fade-in') {
                setAnimation({
                    fade: 'animate-fade-out'
               })
            } else {
                setAnimation({
                    fade: 'animate-fade-in'
                 })
            }
         }, 2000);
        return () => clearInterval(timeout)
    }, [data]);

    const getData = async (location) => {
        await axios.get(baseURL+location.pathname+location.search)
        .then(res => {
            setData(res.data)
        }).catch(err => {
            console.log(err);
        });
    }
    const getFilters = async (location) => {
        let slug = location.search.split('=');
        const req = slug.length < 2 ? slug[1] : slug[1].split('&')[0];
        await axios.get(baseURL+'/menu/categories/'+req)
        .then(res => {
            setFilter(res.data)
        }).catch(err => {
            console.log(err);
        });
    }
    return (
        <>
            <Nav/>
            <div className="flex flex-col gap-y-8 justify-center items-center mt-5">
                <div className="flex flex-row gap-x-6 justify-center items-center mb-5">
                    {filter.types && filter.types.map(items => (
                        <Filters key={items.id} title={items.title} slug={items.slug} cat_slug={filter.slug}></Filters>
                    ))
                    }
                </div>
                {data.map(item => (
                    <Link className={animation.fade} key={item.slug} to={location.pathname+item.slug}>
                        <Cards  key={item.slug} color={{'primary': item.colors.primary, 'secondary' : item.colors.secondary}} img={item.media && item.media.img} price_bottle={item.price_bottle} price_glass={item.price_glass} title={item.title} origin={item.origin}></Cards>
                    </Link>
                ))
                }
                <Footer/>
            </div>
        </>
    )
}

export default Catalogue
