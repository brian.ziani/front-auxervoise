import React from 'react'

const  Cards = ({color, title, price_bottle, price_glass, img, origin}) => {
    return (
        <div className='animation-fade-in w-80 h-32 rounded-xl flex-row z-0' style={{background: color.primary}}>
            <div className="w-20 h-20 rounded-bl-full rounded-tr-special float-right" style={{background: color.secondary}}>
                <div className='h-36 w-36 transform -translate-x-12 -translate-y-4 m-0'>
                    { img ? <img className='h-36 w-full m-0' src={img}></img>
                        : 
                       <div className=" w-full h-full flex justify-center">
                            <svg className='w-28 h-28 m-0 text-white self-center' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                                <path fillRule="evenodd" d="M4 5a2 2 0 00-2 2v8a2 2 0 002 2h12a2 2 0 002-2V7a2 2 0 00-2-2h-1.586a1 1 0 01-.707-.293l-1.121-1.121A2 2 0 0011.172 3H8.828a2 2 0 00-1.414.586L6.293 4.707A1 1 0 015.586 5H4zm6 9a3 3 0 100-6 3 3 0 000 6z" clipRule="evenodd" />
                            </svg>
                       </div>
                        }
                </div>
            </div>
            <div className="h-28 w-full text-left justify-self-end p-4 text-white leading-relaxed">
                <h3 className="text-base">{title}</h3>
                {origin != null ? <p className="text-sm">Origine : {origin.title}</p> : <p className="text-sm">Origine : Inconnue</p>}
                <section className="flex flex-row flex-nowrap gap-3 justify-items-center">
                    {price_glass && <div className="flex flex-col">
                        <div className="rounded-full text-center w-16 py-1 px-2 text-sm mt-2 text-black bg-gray-50"><p>{price_glass} €</p></div>
                    </div>}
                    {price_bottle && <div className="flex flex-col">
                        <div className="rounded-full text-center w-16 py-1 px-2 text-sm mt-2 text-black bg-gray-50"><p>{price_bottle} €</p></div>
                    </div>}
                </section>
            </div>
        </div>
    )
}

export default Cards
