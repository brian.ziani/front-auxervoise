import React, {useEffect, useState} from 'react'
import {NavLink} from 'react-router-dom';
import {useLocation} from 'react-router-dom';

const Filters = ({title, slug, action}) => {
    let location = useLocation();
    const query = location.search.split('&');
    const [animation, setAnimation] = useState({fade : 'animate-fade-in'})

    useEffect(() => {setAnimation({fade : 'animate-fade-in'})}, [location])

    return (
        <NavLink onClick={action} className='text-black text-center text-base' to={{pathname: "/menu/catalogue/", search:query[0]+"&type="+slug}}>
            <div className='text-black text-center text-base flex items-center justify-center flex-col'>
                {title}
                {
                    location.search.includes("&type="+slug) && <div className={`rounded-full h-2 w-2 bg-yellow-300 ${animation.fade}`}></div>
                }
            </div>
        </NavLink>
    )
}

export default Filters
