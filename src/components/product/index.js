import React, {useEffect, useState} from 'react'
import axios from 'axios';
import {useHistory, useLocation} from 'react-router-dom';
import Footer from '../footer'
import {animated, useSpring, config} from 'react-spring';
import { baseURL } from '../../middleware/middleware.axios';
const Product =() => {
    let location = useLocation();
    let history = useHistory();
    const [data, setData] = useState([]);

    const down = useSpring({
        from: {
            y : -480
        },
        to: {
            y : 0
        },
        config: config.slow,
    });
    const up = useSpring({
        from: {
            opacity : 0,
            y : 220,
            x: -60
        },
        to: {
            opacity : 1,
            y : 120,
            x: -60
        },
        delay : 300,
        config: config.molasses,
    });

    useEffect(() => {
        getData(location);
    }, [location]);

    const getData = async (location) => {
        await axios.get(baseURL+location.pathname)
        .then(res => {
            setData(res.data)
        }).catch(err => {
            console.log(err);
        });
    }
    const getHumanDate = (data) => {
        const date = new Date(data)
        const humanDate = date.getDate() + '/' + (date.getMonth()+1) + '/' + date.getFullYear()
        return humanDate; 
    }
    return (
        <div>
            <animated.div className='relative text-white ' style={down}>
                <div className="absolute top-6 p-2 grid grid-cols-12 auto-cols-min items-center w-full text-center ">
                    <svg onClick={() => {history.goBack()}} xmlns="http://www.w3.org/2000/svg" className="h-8 w-8" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M15 19l-7-7 7-7" />
                    </svg>
                    <h2 className="text-lg text-center tracking-wider col-span-10 self-center">{data.product && data.product.title}</h2>
                </div>
                <div className={`h-112 w-full z-0 rounded-bl-9xl grid grid-cols-2`} style={{background: data.product && data.product.colors.primary}}>
                    <div className="rounded-tr-full rounded-bl-special-xl h-72 w-72 absolute -bottom-0 -left-4" style={{background: data.product && data.product.colors.secondary}}></div>
                </div>
                <div className="grid h-96 grid-cols-12 absolute bottom-0">
                    {
                        data.product && data.product.media && data.product.media.img ?
                        <animated.div style={up} className="w-80 h-80 z-1 col-span-7 transform translate-y-28 -translate-x-14">
                            <img src={data.product && data.product.media.img}></img>
                        </animated.div>
                        : 
                        <div  className="w-80 h-80 z-1 col-span-7 transform translate-y-28 p-5">
                            <svg className='w-28 h-28 m-0 text-white self-center' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                                <path fillRule="evenodd" d="M4 5a2 2 0 00-2 2v8a2 2 0 002 2h12a2 2 0 002-2V7a2 2 0 00-2-2h-1.586a1 1 0 01-.707-.293l-1.121-1.121A2 2 0 0011.172 3H8.828a2 2 0 00-1.414.586L6.293 4.707A1 1 0 015.586 5H4zm6 9a3 3 0 100-6 3 3 0 000 6z" clipRule="evenodd" />
                            </svg>
                            <p className="">Photo non disponibles</p>
                        </div>
                    }
                    <div className="col-span-5 grid grid-rows-6 p-2 items-center">
                        <div className="row-span-1"></div>
                        {
                            data.product && 
                            data.product.origin && <div>
                                <h3 className="text-base font-semibold tracking-wider">Origine</h3>
                                <p className="pl-2">{data.product.origin && data.product.origin.title}</p>
                            </div>
                        }
                        {
                            data.product && 
                            data.product.temperature && <div>
                                <h3 className="text-base font-semibold tracking-wider">Degrés</h3>
                                <p className="pl-2">{data.product.temperature && data.product.temperature.title}°</p>
                            </div>
                        }
                        {
                            data.product && data.product.date_of_brew ? 
                            <div>
                                <h3 className="text-base font-semibold tracking-wider">Mis en Bouteille</h3>
                                <p className="pl-2">{getHumanDate(data.product.date_of_brew && data.product.date_of_brew)}</p>
                            </div>
                            :
                            <div>
                                <h3 className="text-base font-semibold tracking-wider">Préparation</h3>
                                <p className="pl-2">{getHumanDate(Date.now())}</p>
                            </div>
                        }
                        {
                            data.product && 
                            <div className="flex flex-row flex-nowrap gap-2">
                                { data.product.price_glass  && <div className="flex flex-col flex-nowrap">
                                    <div className={`rounded-full text-center w-16 py-1 px-2 text-lg mt-2 text-black bg-gray-50`}><p>{data.product.price_glass && data.product.price_glass} €</p></div>
                                </div>}
                                {data.product.price_bottle  && <div className="flex flex-col flex-nowrap">
                                    <div className={`rounded-full text-center w-16 py-1 px-2 text-lg mt-2 text-black bg-gray-50`}><p>{data.product.price_bottle && data.product.price_bottle} €</p></div>
                                </div>}
                            </div>
                        }
                        <div className="row-span-1"></div>
                    </div>
                </div>
            </animated.div>
            {
                data.product &&
                <div className="w-full mt-20 p-5">
                    <h3 className="text-2xl font-bold">Description :</h3>
                    <p className="p-1 text-justify">{data.product.description && data.product.description}</p>
                </div>
            }
            {
                data.contained && data.contained.length > 0 &&
                <div className="w-full p-5">
                    <h3 className="text-2xl font-bold ">Composition :</h3>
                    <div className="grid grid-cols-2 grid-rows-3">
                    {data.contained.map(item => (
                        <div className="col-span-1 p-2" key={item.title}>
                            <p className="" key={item.title}>{item.product}</p>
                            <p className="text-gray-600"> Quantité : {item.quantity}g</p>
                        </div>
                    ))} 
                    </div>
                </div>
            }
            {
                data.product && data.product.infos.length > 0 &&
                <div className="w-full p-5">
                    <h3 className="text-2xl font-bold ">Informations & Allergènes :</h3>
                    {data.product.infos.map(item => (
                        <div className="col-span-1 " key={item.title}>
                            <p className="p-1" key={item.title}><span className="font-bold ">{item.title} : </span>{item.description}</p>
                        </div>
                    ))} 
                </div>
            }
            <Footer/>
        </div>
       
    )
}

export default Product
