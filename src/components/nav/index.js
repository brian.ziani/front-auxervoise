import React, {useState, useEffect} from 'react';
import SideBar from './sideBar';
import '../../styles/style.css';

const Nav = () => {
    const [isOpen, setOpen] = useState(true);
    const toggle = () => {
        setOpen(!isOpen);
    }

    return (
        <div className='grid grid-cols-2 p-2 content-center static z-0'>
            <div className='max-h-10'>
                <svg onClick={toggle} xmlns="http://www.w3.org/2000/svg" className="h-8" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M4 6h16M4 12h8m-8 6h16" />
                </svg>
            </div>
            <div className='float-right'>
                <h1 className='float-right text-xl'>L'Auxervoise</h1>
            </div>
            <SideBar toggle={toggle} open={isOpen}></SideBar>
        </div>
    )
}

export default Nav
