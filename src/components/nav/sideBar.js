import axios from 'axios';
import React, {useEffect, useState}  from 'react';
import {NavLink} from 'react-router-dom';
import {useLocation} from 'react-router-dom';
import { baseURL } from '../../middleware/middleware.axios';
import '../../styles/style.css'

const SideBar = ({toggle, open}) => {
    const [data, setData] = useState([]);
    const location = useLocation();

    useEffect(() => {
        let isMounted = true;   
        getMenuData(isMounted);
        return () => {isMounted = false};
    }, [setData]);
    
    const getMenuData = async (isMounted) => {
        await axios.get(baseURL+'/menu/categories')
        .then(res => {
            if (isMounted) setData(res.data)
        }).catch(err => {
            console.log(err);
        });
    }
    return (
        <div className={`z-50 absolute w-full h-full bg-white transition duration-500 ease-in-out transform ${open ? '-translate-x-full' : '-translate-x-0'}`}>
            <div className='h-20'>
                <svg onClick={toggle} xmlns="http://www.w3.org/2000/svg" className="h-8" viewBox="0 0 20 20" fill="currentColor">
                    <path fillRule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clipRule="evenodd" />
                </svg>
            </div>
            <div className='p-10 text-2xl leading-10 text-justify flex flex-col'>
                <NavLink className='flex items-center' to={{pathname: "/", }}>
                    Recommandations {location.pathname === "/" && <div className="rounded-full h-2 w-2 ml-6 bg-yellow-300"></div>}
                </NavLink>
                {
                    data.map(items => (
                        <NavLink key={items.slug} onClick={toggle} to={{pathname: "/menu/catalogue/", search: "?categories="+items.slug}}>
                            <div className='flex items-center'>{items.title}
                                {location.search.includes("?categories="+items.slug) && <div className="rounded-full h-2 w-2 ml-6 bg-yellow-300"></div>}
                            </div>
                        </NavLink>
                    )) 
                }
            </div>
        </div>
    )
}

export default SideBar
