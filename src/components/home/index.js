import React, {useEffect, useState} from 'react'
import axios from 'axios';
import {Link} from 'react-router-dom';
import Cards from '../catalogue/cards'
import Nav from '../nav'
import Footer from '../footer'
import { baseURL } from '../../middleware/middleware.axios';
const Home = () => {
    const [data, setData] = useState([]);
    const [animation, setAnimation] = useState({fade : 'animate-fade-in'})
    useEffect(() => {
        getData();
    }, []);
    const getData = async () => {
        await axios.get(baseURL+'/menu/accueil')
        .then(res => {
            setData(res.data)
        }).catch(err => {
            console.log(err);
        });
    }
    console.log(data)
    return (
        <>
            <Nav/>
            <h2 className="p-5 text-bold text-xl">Recommandé par l'Auxervoise</h2>
            <div className="flex flex-col gap-y-8 justify-center items-center mt-5">
                {data && data.map(item => (
                    <Link className={animation.fade} key={item.slug} to={"menu/catalogue/"+item.slug}>
                        <Cards  key={item.slug} color={{'primary': item.colors.primary, 'secondary' : item.colors.secondary}} img={item.media && item.media.img} price_bottle={item.price_bottle} price_glass={item.price_glass} title={item.title} origin={item.origin}></Cards>
                    </Link>
                ))
                }
            </div>
            <Footer/>
        </>
    )
}

export default Home
