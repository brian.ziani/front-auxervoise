const webpack = require('webpack');
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CompressionPlugin = require("compression-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const { extendDefaultPlugins } = require('svgo');
const zlib = require("zlib");
const path = require('path');
const dotenv = require('dotenv').config({path: __dirname + '/.env'});
const ImageMinimizerPlugin = require('image-minimizer-webpack-plugin');

module.exports = {
    entry: path.resolve(__dirname, 'src/') + path.join('/index.js'),
    output: {
        path : path.resolve(__dirname, 'dist'),
        filename : 'bundle.[name].js', 
        publicPath: '/',
    },
    module: {
        rules: [
            {
                test: /(\.js[\S]{0,1})$/i,
                include: path.resolve(__dirname, 'src'),
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env','@babel/react'],
                    },
                },
            },
            {
              test: /\.css$/i,
              include: path.resolve(__dirname, 'src/styles'),
              use: [
                MiniCssExtractPlugin.loader,
                "css-loader", "postcss-loader",
                ],
            },
            {
              test: /\.(png|jpg|svg|gif)$/i,
              loader: 'file-loader',
              options: {
                name: '[name][contenthash].[ext]',
                outputPath: 'img',
              },
            },  
            {
              test: /\.(woff|woff2|eot|ttf|otf)$/i,
              loader: 'file-loader',
              options: {
                name: '[name][contenthash].[ext]',
                outputPath: 'fonts',
              },
            },    
        ],
    },
    plugins: [
        new MiniCssExtractPlugin({
          filename: "styles.css",
          chunkFilename: "styles.css"
        }),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, "index.html")
          }),
          new webpack.ids.HashedModuleIdsPlugin(),
          new webpack.DefinePlugin({
            "process.env": JSON.stringify(dotenv.parsed)
          }),
          new webpack.ProvidePlugin({
            "React": "react", 'react-dom': 'ReactDOM'
          }),
          new CompressionPlugin({
            filename: "[path][base].br",
            algorithm: "brotliCompress",
            test: /\.(js|css|html)$/,
            compressionOptions: {
              params: {
                [zlib.constants.BROTLI_PARAM_QUALITY]: 11,
              },
            },
            threshold: 10240,
            minRatio: 0.8,
            deleteOriginalAssets: false,
          }),
        new ImageMinimizerPlugin({
            minimizerOptions: {
              // Lossless optimization with custom option
              // Feel free to experiment with options for better result for you
              plugins: [
                ['mozjpeg', { progressive: true }],
                ['pngquant', { optimizationLevel: 6 }],
                [
                  'svgo',
                  {
                    plugins: extendDefaultPlugins([
                      {
                        name: 'removeViewBox',
                        active: false
                      },
                    ]),
                  },
                ],
              ],
            },
        }),
    ],
    resolve: {
      extensions: ['.js', '.es6', '.jsx', 'less','config','variables','overrides']
    },    
    performance: {
      hints: false
    }, 
    optimization: {
        runtimeChunk: 'single',
        splitChunks: {
          chunks: 'all',
          maxInitialRequests: Infinity,
          minSize: 0,
          cacheGroups: {
            vendor: {
              test: /[\\/]node_modules[\\/]/,
              name(module) {
                // get the name. E.g. node_modules/packageName/not/this/part.js
                // or node_modules/packageName
                const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];
    
                // npm package names are URL-safe, but some servers don't like @ symbols
                return `npm.${packageName.replace('@', '')}`;
              },
            },
          },
        },
    },
    devServer : {
        contentBase: path.join(__dirname),
        compress: true,
        host: '127.0.0.1',
        hot: true,
        port: 9000,
        historyApiFallback: true,
    },
    externals: [
      {pg: true}
    ],
};